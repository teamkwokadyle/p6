/*
 * CSc103 Project 5: Syntax highlighting, part two.
 * See readme.html for details.
 * Please list all references you made use of in order to complete the
 * assignment: your classmates, websites, etc.  Aside from the lecture notes
 * and the book, please list everything.  And remember- citing a source does
 * NOT mean it is okay to COPY THAT SOURCE.  What you submit here **MUST BE
 * YOUR OWN WORK**.
 * References:
 *
 *
 * Finally, please indicate approximately how many hours you spent on this:
 * #hours:
 */

#include "fsm.h"
using namespace cppfsm;
#include <iostream>
using std::cin;
using std::cout;
using std::endl;
#include <string>
using std::string;
#include <set>
using std::set;
#include <map>
using std::map;
#include <initializer_list> // for setting up maps without constructors.
#include <vector>
using std::vector;

// enumeration for our highlighting tags:
enum {
	hlstatement,  // used for "if,else,for,while" etc...
	hlcomment,    // for comments
	hlstrlit,     // for string literals
	hlpreproc,    // for preprocessor directives (e.g., #include)
	hltype,       // for datatypes and similar (e.g. int, char, double)
	hlnumeric,    // for numeric literals (e.g. 1234)
	hlescseq,     // for escape sequences
	hlerror,      // for parse errors, like a bad numeric or invalid escape
	hlident       // for other identifiers.  Probably won't use this.
};

// usually global variables are a bad thing, but for simplicity,
// we'll make an exception here.
// initialize our map with the keywords from our list:
map<string, short> hlmap = {
#include "res/keywords.txt"
};
// note: the above is not a very standard use of #include...

// map of highlighting spans:
map<int, string> hlspans = {
	{hlstatement, "<span class='statement'>"},
	{hlcomment, "<span class='comment'>"},
	{hlstrlit, "<span class='strlit'>"},
	{hlpreproc, "<span class='preproc'>"},
	{hltype, "<span class='type'>"},
	{hlnumeric, "<span class='numeric'>"},
	{hlescseq, "<span class='escseq'>"},
	{hlerror, "<span class='error'>"}
};
// note: initializing maps as above requires the -std=c++0x compiler flag,
// as well as #include<initializer_list>.  Very convenient though.
// to save some typing, store a variable for the end of these tags:
string spanend = "</span>";

string translateHTMLReserved(char c) {
	switch (c) {
		case '"':
			return "&quot;";
		case '\'':
			return "&apos;";
		case '&':
			return "&amp;";
		case '<':
			return "&lt;";
		case '>':
			return "&gt;";
		case '\t': // make tabs 4 spaces instead.
			return "&nbsp;&nbsp;&nbsp;&nbsp;";
		default:
			char s[2] = {c,0};
			return s;
	}
}

void testFSM(string s, vector<int>& fsmret){
	int cstate = start;
	for(unsigned long i = 0; i < s.length(); i++)
	{
		fsmret.push_back(updateState(cstate, s[i]));
	}
	fsmret.push_back(cstate);
}

	void sub(string input, vector <int>& fsmret) {
		string sub, subnew, output;

		for (int i = 1; i < fsmret.size(); i++) {
			sub += input[i-1];

			//for tabs in the beginning
			if (fsmret[i] == 0 && fsmret[i-1] != 3) { //to account for the inclusion
																								//of the ending " in a string
																								//literal
				output += translateHTMLReserved(input[i-1]);
			}

			if (fsmret[i] != fsmret[i+1]) {

			//for comments
			if (fsmret[i] == 4 && fsmret[i+1] == 2) {
				for (int k = i; k < input.size(); k++) {
					sub += translateHTMLReserved(input[k]);
				}
				output += hlspans[hlcomment] + sub + spanend;
				break;
			}

			//for string literals
			else if (fsmret[i] == 3) {

				//for escape characters
				if (fsmret[i-1] == 5) { //for escape chars
					sub = input[i-2] + sub;
					output += hlspans[hlescseq] + sub + spanend;
				}
				if (fsmret[i+1] == 0 && fsmret[i-1] == 5) {
					sub = input[i]; //adds the last "
					for (int k = 0; k < sub.size(); k++) {
						subnew += translateHTMLReserved(sub[k]);
					}
					output += hlspans[hlstrlit] + subnew + spanend;
				}
				else {
					if (input[i] != '\\') sub += input[i];
					for (int k = 0; k < sub.size(); k++) {
						subnew += translateHTMLReserved(sub[k]);
					}
					output += hlspans[hlstrlit] + subnew + spanend;
				}
			}
			//for statements
			else if(fsmret[i] == 1){
				map<string, short>::iterator it;
				it = hlmap.find(sub);
				if(it != hlmap.end()){
					for(int k = 0; k <sub.size();k++){
						subnew += translateHTMLReserved(sub[k]);
					}
					output += hlspans[hlmap[sub]] + subnew +spanend;
				}
				else output += sub;
			}
			//for numbers
			else if (fsmret[i] == 6) {
				output += hlspans[hlnumeric] + sub + spanend;
				if (fsmret[i+1] == 7) {
					for (int k = i; k <input.size(); k++) {
						subnew += translateHTMLReserved(input[k]);
					}
					output += hlspans[hlerror] + subnew + spanend;
					break;
				}
			}
			//for errors
			else if (fsmret[i] == 5 && fsmret[i+7] == 7){
				for (int k = i; k <input.size(); k++){
					sub += translateHTMLReserved(input[k]);
				}
				output += hlspans[hlerror] + sub + spanend;
				break;
			}
				//reset subnew and sub
				subnew.erase(subnew.begin(), subnew.begin()+subnew.size());
				sub.erase(sub.begin(), sub.begin()+sub.size());
				}
			}
			cout << output << endl;
		}

int main() {
	// TODO: write the main program.
	// It may be helpful to break this down and write
	// a function that processes a single line, which
	// you repeatedly call from main().
	string input;
	vector <int> fsmret;

	while (getline(cin,input)) {

	testFSM(input,fsmret); //to get fsmret
	sub(input,fsmret); //where the output is coming from
	fsmret.erase(fsmret.begin(), fsmret.end()); //to clear fsmret for new lines
// for (int i = 0, i <= input; i++){
// 		if (input[i] = 'start')
// 			return "id delim";
// 		else if (input[i] = 'scan idfr')
// 			return "ident:";
// 		else return 0;
// 	}
	}
	return 0;
}
